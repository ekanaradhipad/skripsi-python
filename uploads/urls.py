from django.conf.urls import url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from uploads.core import views


urlpatterns = [
    #url(r'^$', views.home, name='home'),
    #url(r'^uploads/simple/$', views.simple_upload, name='simple_upload'),
    url(r'^crawl/', views.crawl, name='crawl'),
    url(r'^$', views.simple_upload, name='simple_upload'),
    url(r'^admin/', admin.site.urls),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
