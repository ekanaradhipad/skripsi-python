from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from django.core.files import File

from uploads.core.models import Document

from filebrowser.sites import site
from filebrowser.base import FileObject
from filebrowser.base import FileListing

import urllib, json

from converter.csvw import build_schema, CSVWConverter

from rdflib import URIRef, Namespace

from rdflib import BNode, Literal

import fnmatch

import rdflib
import os
from rdflib.graph import ConjunctiveGraph as Graph
from rdflib import plugin
from rdflib.store import Store
from rdflib.store import NO_STORE
from rdflib.store import VALID_STORE
from rdflib import Literal
from rdflib import Namespace
from rdflib import URIRef

from pymantic import sparql

import converter_helper

from django.http import HttpResponse


from rdflib.plugins.stores import sparqlstore

import requests 

from tempfile import mkdtemp

import sparql_helper
graph_counter = 0


def crawl(request):

    web = request.POST.get("web", None)

    #IP = get_ip()
    #IP = '192.168.0.15'
    IP = 'localhost'

    counter = 0
    fail = 0

    if web is not None:
        #url = "http://" + web + "/api/3/action/package_search?q=groups:pendidikan&rows=" + limit
        limit = request.POST.get("limit", None)
        group = request.POST.get("group", None)
        keyword = request.POST.get("keyword", None) 
        use_group = request.POST.get("use_group", None)

        #query_endpoint = 'http://localhost:3030/:fuseki.war/data'
        #sparql_endpoint = 'http://localhost:3030/:fuseki.war/sparql'
        query_endpoint = 'http://' + IP + ':3030/ds/data'
        sparql_endpoint = 'http://' + IP + ':3030/ds/sparql'

        files_processed = []
        mapping_produced = []
        rdf_produced = []
        files_failed = []

        print("group:" + group)
        print("keyword:" + keyword)
        
        url = "http://" + web + "/api/3/action/package_search?" + "rows=" + limit + "&"
    
        url = url + "q=" + group +  keyword + "&"

        print url

        fs = FileSystemStorage()

        response = urllib.urlopen(url)
        data = json.loads(response.read())
        length = len(data['result']['results']) 

        print("OK!")

        print("limit given = " + str(length))

        for x in range(length):

            resources_count = len(data['result']['results'][x]["resources"])

            print("count: " + str(resources_count))
    
            g_table_id = sparql_helper.get_group_table_id()

            group_table_uri = 'http://od2rdf.cs.ui.ac.id/flat/entity/G' + g_table_id

            group_table = URIRef(group_table_uri)

            author = str(web) + '/' + str(data['result']['results'][x]["organization"]['name'])
            
            data_url = ''

            for y in range(resources_count):
                try:

                    #r = requests.post("http://google.co.id", data={})

                    table_id = sparql_helper.get_table_id()
                    data_url = data['result']['results'][x]["resources"][y]['url']

                    print('data: ' + data_url)

                    filename = data['result']['results'][x]["resources"][y]['name']
                    filename = filename  + ".csv"
                    filename = filename.replace(' ', '_')

                    mapping_file = "{}-metadata.json".format(filename)
                    rdf_target_file = "{}.nq".format(filename)
                    final_output = "{}.ttl".format(filename)

                    created = data['result']['results'][x]["resources"][y]['created']
                    desc = data['result']['results'][x]["resources"][y]['description']
                    file = urllib.urlretrieve(data_url, filename)
                    o_table_id = data['result']['results'][x]["resources"][y]['id']
                    o_group_table_id = data['result']['results'][x]["resources"][y]['package_id']

                    
                    graph_uri = sparql_helper.graph_uri_creator(table_id)

                    base = "https://od2rdf.cs.ui.ac.id/flat/" 

                    converter_helper.csv_to_json(csv_file= open(filename), mapping_file = mapping_file, url = filename, base = base)

                    with open(mapping_file) as f:
                    #converter_helper.edit_property(f)
                        json_save = fs.save(mapping_file, f)
                        json_url = fs.url(json_save)
                        mapping_produced.append(json_url)

                    converter_helper.edit_property(mapping_file = fs.open(mapping_file, 'r+'), id = table_id, base = base)

                    converter_helper.csv_json_to_nq(file_name=open(filename) , target_file=rdf_target_file , schema_file_name=fs.open(mapping_file))

                    with open(rdf_target_file) as f:
                        graph = sparql_helper.graph_creator(f, graph_uri)

                    #graph = converter_helper.graph_bind(graph = graph, mapping_file = fs.open(mapping_file)) 
                    dublin = Namespace('http://purl.org/dc/terms/')
                    prop = Namespace('http://od2rdf.cs.ui.ac.id/prop')
                    table = Namespace('http://od2rdf.cs.ui.ac.id/flat/entity/table')
                    subject = URIRef(graph_uri)
                    user = URIRef('od2rdf.cs.ui.ac.id/user')
                    vocab = Namespace('https://od2rdf.cs.ui.ac.id/flat/vocab/')
                    prov =Namespace('http://www.w3.org/ns/prov/')
                    od_prop = Namespace('https://od2rdf.cs.ui.ac.id/metadata/prop/')

                    graph.add((subject, dublin.source, URIRef(data_url)))
                    graph.add((subject, dublin.isPartOf, group_table))
                    graph.add((subject, dublin.created, Literal(created)))
                    graph.add((subject, dublin.publisher, URIRef(web)))


                    graph.add((subject, dublin.identifier, Literal(o_table_id)))
                    graph.add((group_table, dublin.identifier, Literal(o_group_table_id)))
                    graph.add((group_table, od_prop.hasAuthor, URIRef(author)))
                    graph.add((subject, od_prop.url, URIRef(url)))

                    print ("PREPARE SERIALIZE")
                    graph.serialize(destination=final_output, format='turtle')
                    print("FINISH")

                    total_triples = len(graph)
         
                    with open(final_output) as f:

                        rdf= fs.save(final_output, f)
                        rdf_url = fs.url(rdf)
                        rdf_produced.append( rdf_url)
                    
                    try:
                        sparql_helper.rdf_to_store(query_endpoint= query_endpoint, sparql_endpoint = sparql_endpoint, graph = graph, graph_uri = graph_uri)

                        triples_in_graph = sparql_helper.triple_count(query_endpoint= query_endpoint, sparql_endpoint = sparql_endpoint)
                    except Exception as e:
                        print("Error Store : " + str(e))
                        pass

                    try:
                        triples_in_graph = sparql_helper.triple_count(query_endpoint= query_endpoint, sparql_endpoint = sparql_endpoint)
                    except Exception as e:
                        print("Error Store Count : " + str(e))
                        pass

                    print(desc)
                    counter = counter + 1

                    files_processed.append(filename)

                    if os.path.exists(rdf_target_file):
                     os.remove(rdf_target_file)

                    if os.path.exists(mapping_file):
                     os.remove(mapping_file)

                    if os.path.exists(filename):
                     os.remove(filename)

                    if os.path.exists(final_output):
                     os.remove(final_output)

                except Exception as error:
                            print error

                            if os.path.exists(rdf_target_file):
                             os.remove(rdf_target_file)

                            if os.path.exists(mapping_file):
                             os.remove(mapping_file)

                            if os.path.exists(final_output):
                             os.remove(final_output)

                            if os.path.exists(filename):
                             os.remove(filename)

                            fail = fail + 1

                            files_failed.append(data_url)
                            pass
                        
            print("counter = " + str(counter))
            if os.path.exists(mapping_file):
             os.remove(mapping_file)

            if os.path.exists(rdf_target_file):
             os.remove(rdf_target_file)

            if os.path.exists(final_output):
             os.remove(final_output)

            if os.path.exists(filename):
             os.remove(filename)

            if os.path.exists(final_output):
             os.remove(final_output)


        print "Download Complete!"

        print("files:" + str(len(files_processed)))
        print("mapping_produced:" + str(len(mapping_produced)))
        print("rdf_produced:" + str(len(rdf_produced)))


        return render(request, 'core/crawl.html', {
            'counter': counter, 
            'fail' : fail,
            'url' : url,
            'files_processed' : files_processed,
            'mapping_produced' : mapping_produced,
            'rdf_produced' : rdf_produced,
            'files_failed' : files_failed
        })

    return render(request, 'core/crawl.html')


def simple_upload(request):
    uploaded_file_url = "-"
    json_url = "-"
    rdf_url = "-"
    total_triples = "-"
    triples_in_graph = "-"
    documents = "-"
    message = ""
    #IP = get_ip()
    IP = 'localhost:3030'
    #IP = '192.168.0.15'
    #IP = 'afternoon-bayou-79151.herokuapp.com'
    fs = FileSystemStorage()
    #query_endpoint = 'http://localhost:3030/ds/data'
    #sparql_endpoint = 'http://localhost:3030/ds/sparql'

    query_endpoint = 'http://' + IP + '/ds/data'
    sparql_endpoint = 'http://' + IP + '/ds/sparql'

    #query_endpoint = 'http://localhost:3030/ds/data'
    #sparql_endpoint = 'http://localhost:3030/ds/sparql'


    is_exist = request.FILES.get('csv_to_rdf', False)
    opm = request.POST.get("checks", None)
    #Sstore = request.POST.get("checks_store", None)
    print "opm: " + str(opm)
    #print is_exist
    if request.method == 'POST' and is_exist:
        myfile = request.FILES['csv_to_rdf']

        filename = (myfile.name).replace(' ', '_')
        
        filename = fs.save(filename, myfile)
        uploaded_file_url = filename
        json_url = ""
       # print fs.exists(filename)
        mapping_file = "{}-metadata.json".format(filename)
        rdf_target_file = "{}.nq".format(filename)
        final_output = "{}.ttl".format(filename)

        uploaded_file_url = final_output
        
        table_id = sparql_helper. get_table_id()
        graph_uri = sparql_helper.graph_uri_creator(table_id)

        base = "http://od2rdf.cs.ui.ac.id/flat/" 

        converter_helper.csv_to_json(csv_file= fs.open(filename), mapping_file = mapping_file, url = filename, base = base)
        
        with open(mapping_file) as f:
            #converter_helper.edit_property(f)
            json= fs.save(mapping_file, f)
            json_url = fs.url(json)

        converter_helper.edit_property(mapping_file = fs.open(mapping_file, 'r+'), id = table_id, base = base)

        try:
            converter_helper.csv_json_to_nq(file_name=fs.open(filename) , target_file=rdf_target_file , schema_file_name=fs.open(mapping_file))

        except Exception as error:
            if os.path.exists(mapping_file):
             os.remove(mapping_file)

            if os.path.exists(rdf_target_file):
             os.remove(rdf_target_file)

            if os.path.exists(final_output):
             os.remove(final_output)

            #fs.delete(filename) 
            #fs.delete(mapping_file)

            filelisting = FileListing(settings.MEDIA_ROOT, sorting_by='date', sorting_order='desc')

            documents = []
            documents2 = []

            for file in filelisting.listing():
                if fnmatch.fnmatch(file, '*.ttl'):
                    documents.append(fs.url(file))

            for file in filelisting.listing():
                if fnmatch.fnmatch(file, '*.json'):
                    documents2.append(fs.url(file))

            message = "An error has occured, please check the csv file"

            return render(request, 'core/simple_upload.html', {
            'triples_in_graph' : triples_in_graph,
            'documents': documents,
            'documents2': documents2,
            'message' : message
        })

        with open(rdf_target_file) as f:

            graph = sparql_helper.graph_creator(f, graph_uri)

        dublin = Namespace('http://purl.org/dc/terms/')
        subject = URIRef(graph_uri)
        user = URIRef('od2rdf.cs.ui.ac.id/user')
        graph.add((subject, dublin.source, user)  )
        graph.serialize(destination=final_output, format='turtle')

        total_triples = len(graph)

        if os.path.exists(rdf_target_file):
         os.remove(rdf_target_file)

        if opm is not None:

            message = "Maping file succesfully created"

            filelisting = FileListing(settings.MEDIA_ROOT, sorting_by='date', sorting_order='desc')

            documents = []
            documents2 = []

            for file in filelisting.listing():
                if fnmatch.fnmatch(file, '*.ttl'):
                    documents.append(fs.url(file))

            for file in filelisting.listing():
                if fnmatch.fnmatch(file, '*.json'):
                    documents2.append(fs.url(file))

            fs.delete(filename)

            return render(request, 'core/simple_upload.html', {
            'uploaded_file_url': uploaded_file_url, 
            'json_url': json_url,
            'rdf_url': rdf_url,
            'total_triples' : total_triples,
            'triples_in_graph' : triples_in_graph,
            'documents': documents,
            'message' : message
        })
           
        with open(final_output) as f:

            rdf= fs.save(final_output, f)
            rdf_url = fs.url(rdf)

        try:
            sparql_helper.rdf_to_store(query_endpoint= query_endpoint, sparql_endpoint = sparql_endpoint, graph = graph, graph_uri = graph_uri)

            triples_in_graph = sparql_helper.triple_count(query_endpoint= query_endpoint, sparql_endpoint = sparql_endpoint)
        except Exception as e:
            print("Error Store : " + str(e))
            pass
        
        try:
            triples_in_graph = sparql_helper.triple_count(query_endpoint= query_endpoint, sparql_endpoint = sparql_endpoint)
        except Exception as e:
            print("Error Store Count : " + str(e))
            pass

        if os.path.exists(mapping_file):
         os.remove(mapping_file)

        if os.path.exists(rdf_target_file):
         os.remove(rdf_target_file)

        if os.path.exists(final_output):
         os.remove(final_output)

        fs.delete(filename) 

        filelisting = FileListing(settings.MEDIA_ROOT, sorting_by='date', sorting_order='desc')

        documents = []
        documents2 = []
        message = "Maping and RDF file succesfully created"

        for file in filelisting.listing():
            if fnmatch.fnmatch(file, '*.ttl'):
                documents.append(fs.url(file))

        for file in filelisting.listing():
            if fnmatch.fnmatch(file, '*.json'):
                documents2.append(fs.url(file))

        return render(request, 'core/simple_upload.html', {
            'uploaded_file_url': uploaded_file_url, 
            'json_url': json_url,
            'rdf_url': rdf_url,
            'total_triples' : total_triples,
            'triples_in_graph' : triples_in_graph,
            'documents': documents,
            'documents2': documents2,
            'message' : message
        })

    is_exist = request.FILES.get('csv_to_json_meta_csv', False);
    
    if request.method == 'POST' and is_exist:
        myfile = request.FILES['csv_to_json_meta_csv']
        
        filename = (myfile.name).replace(' ', '_')
        file = fs.save(filename, myfile)
        uploaded_file_url = fs.url(file)

        myfile2 = request.FILES['csv_to_json_meta_json']
        
        filename2 = fs.save(myfile2.name, myfile2)
        json_url = fs.url(filename)
        
        rdf_target_file = "{}.nq".format(filename)
        base = "https://od2rdf.cs.ui.ac.id"

        converter_helper.csv_json_to_nq(file_name=fs.open(filename) , target_file=rdf_target_file , schema_file_name=fs.open(myfile2.name))

        with open(rdf_target_file) as f:
            rdf= fs.save(rdf_target_file, f)
            rdf_url = fs.url(rdf)
        
        table_id = sparql_helper.get_table_id()
        base = sparql_helper.graph_uri_creator(table_id)

        graph = sparql_helper.graph_creator(fs.open(rdf_target_file), base)

        total_triples = len(graph)

        if os.path.exists(rdf_target_file):
         os.remove(rdf_target_file)

        fs.delete(filename) 

        filelisting = FileListing(settings.MEDIA_ROOT, sorting_by='date', sorting_order='desc')

        message = "Maping file succesfully created"

        documents = []
        documents2 = []

        for file in filelisting.listing():
            if fnmatch.fnmatch(file, '*.ttl'):
                documents.append(fs.url(file))

        for file in filelisting.listing():
            if fnmatch.fnmatch(file, '*.json'):
                documents2.append(fs.url(file))
 
        return render(request, 'core/simple_upload.html', {
            'uploaded_file_url': uploaded_file_url, 
            'json_url': json_url,
            'rdf_url': rdf_url,
            'total_triples' : total_triples,
            'triples_in_graph' : triples_in_graph,
            'documents': documents,
            'documents2': documents2,
            'message' : message

        })

    filelisting = FileListing(settings.MEDIA_ROOT, sorting_by='date', sorting_order='desc')

    documents = []
    documents2 = []

    for file in filelisting.listing():
        if fnmatch.fnmatch(file, '*.ttl'):
            documents.append(fs.url(file))

    for file in filelisting.listing():
        if fnmatch.fnmatch(file, '*.json'):
            documents2.append(fs.url(file))

    return render(request, 'core/simple_upload.html', {
           
            'documents': documents,
            'documents2': documents2,
            'message' : message
        })
#def myiter():
    #enqueue_some_task()
 #   return

#def myview(request):
 #   return HttpResponse(myiter())

import socket
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    print("IP is " + str(IP))
    return IP