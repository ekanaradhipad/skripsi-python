import uuid
import json

from rdflib import URIRef, Namespace

import rdflib
import os
from rdflib.graph import ConjunctiveGraph as Graph
from rdflib import plugin
from rdflib.store import Store
from rdflib.store import NO_STORE
from rdflib.store import VALID_STORE
from rdflib import Literal
from rdflib import Namespace
from rdflib import URIRef

from pymantic import sparql
from rdflib.plugins.stores import sparqlstore
from rdflib.namespace import RDF, FOAF


def graph_creator(rdf_file, graph_uri):
    
    graph = Graph(
              identifier = URIRef(graph_uri))
 
    graph.remove((None, None, None))

    with rdf_file as data:
     graph.parse(data, format="nquads")
    
    graph.commit()

    return graph

def graph_uri_creator(table_id):
    
    base_graph_uri = "http://od2rdf.cs.ui.ac.id/flat/" 
    graph_uri = base_graph_uri +  'entity/T' + table_id
    return graph_uri

def rdf_to_store(sparql_endpoint, query_endpoint, graph_uri, graph):
    ugs = sparql.UpdateableGraphStore(sparql_endpoint, query_endpoint)
    #print ugs.text
    ugs.post(graph_uri = graph_uri, graph = graph)

def triple_count(sparql_endpoint, query_endpoint):
    
    query = """SELECT (COUNT(?s) AS ?triples) 
        WHERE {
        GRAPH ?g {
        ?s ?p ?o
        }
        }"""

    store = sparqlstore.SPARQLUpdateStore()
    store.open((sparql_endpoint, query_endpoint))
    
    qres = store.query(query)
    triples_in_fuseki = ""

    for row in qres.result:      
        triples_in_fuseki = row

    triples_in_fuseki = (str(triples_in_fuseki)).split("'")[1]
    print "triples : " + str(triples_in_fuseki)
    return triples_in_fuseki


def get_table_id():

    table_file = 'config_table_id.txt'

    with open(table_file, 'r') as table:
        read = table.read()
        read = int(read)

    with open(table_file, 'w') as table:
        table.write(str(read + 1))

    return str(read)

def get_group_table_id():

    gtable_file = 'config_gtable_id.txt'

    with open(gtable_file, 'r') as gtable:
        read = gtable.read()
        read = int(read)

    with open(gtable_file, 'w') as gtable:
        gtable.write(str(read + 1))

    return str(read)


    