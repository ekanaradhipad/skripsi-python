from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage

from django.core.files import File

from uploads.core.models import Document

from filebrowser.sites import site
from filebrowser.base import FileObject
from filebrowser.base import FileListing

from converter.csvw import build_schema, CSVWConverter

import os
from rdflib import plugin
from rdflib import Literal
from rdflib import Namespace
from rdflib import URIRef
import json

from rdflib import URIRef, Namespace

import rdflib
import os
from rdflib.graph import ConjunctiveGraph as Graph
from rdflib import plugin

import requests 

def csv_to_json(csv_file, mapping_file , url, base):
    build_schema(csv_file, mapping_file, url = url, encoding='utf-8', base = base)
    return 

def csv_json_to_nq(file_name , target_file , schema_file_name):
    c = CSVWConverter(file_name=file_name , target_file=target_file , schema_file_name=schema_file_name, delimiter=',', encoding='utf-8')
    c.convert()
    return

def graph_bind(graph, mapping_file ):

    data = json.load(mapping_file)

    prefix_list = data['@context'][2]

    for content in prefix_list:
        graph.bind(content, prefix_list[content])

    return graph

def edit_property(mapping_file, id, base):

    f = mapping_file
    data = json.load(f)
    #length = len(data['@context'])

    data['@id'] = base + 'entity/T' + id
    data['tableSchema']['aboutUrl'] = "entity/T" + str(id) + "/R{_row}"
    col = data['tableSchema']['columns']
    col_length = len(col)

    for x in range(col_length):
        name = (str(col[x]['name'])).replace(" ", "_")
        col[x]['propertyUrl'] = 'prop/T' + id + '/' + name
        col[x]['@id'] = base + 'prop/T' + id + '/' + name

    f.seek(0)        # <--- should reset file position to the beginning.
    json.dump(data,f, indent = 4)
    f.truncate()   


   




